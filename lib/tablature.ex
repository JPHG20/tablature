defmodule Tablature do

  def parse(tab) do

    tab
    |>String.split("\n\n")
    |> Enum.map(fn x-> parse_part(x)end)
    |>Enum.join(" ")
  end
  def parse_part(tab) do
    tab
     |> String.split()
     |> Enum.map(fn line -> parse_line(line) end)
     |> List.flatten
     |> Enum.group_by(fn {_a,_b,c}->c end)
     |> Enum.sort_by(fn {a,_b}->a end)
     |> Enum.map(fn {_a,b}->b end)
     |> Enum.map(fn x->Enum.map(x,fn {a,b,_c}-> {a,b} end) end )
     |> Enum.map(fn x->Enum.map(x,fn {a,b}->if b =~ ~r/\d+/, do: a<>b, else: "_"  end)end)
     |> Enum.map(fn x->Enum.uniq(x)end)
     |> Enum.map(fn x -> if length(x) > 1, do: x-- ["_"], else: x end)
     |> Enum.map(fn x-> if length(x)>1, do: Enum.join(x,"/"), else: x end )

    #  |>Enum.map(fn {_a,b}->b end)
    #  |>Enum.map(fn x->Enum.map(x,fn {a,_b}-> a end) |>Enum.join("/") end)

    #  #|> Enum.zip
    #  #|> Enum.map(fn {e1, e2, e3} -> "#{e1} #{e2} #{e3}" end)
      |> Enum.join(" ")

  end

   def parse_line(line) do
    line
    |>String.split("")
    |> Enum.with_index()
    |> Enum.filter(fn {_a,b}-> rem(b,2)==0 end )
    |>Enum.filter(fn{a,_b,}-> a != "|" and a != "" end)

    |> Enum.map(fn{a,b} -> {String.at(line,0) ,a,b} end)


    # |>Enum.with_index()
    # |> Enum.filter(fn {a,_b} -> a =~ ~r/\d+/ end)


    #    Regex.scan(~r/\d/, line)
    #  |> List.flatten

    #  |> Enum.with_index()
   end


end
